import GameLooper from './GameLooper.js'
import GameState from './GameState.js'
import GameTime from './GameTime.js'
import { createCanvas, coverScreen, dimensions } from './canvas.js'

/**
 * Truncates a floating point number to the specified number of places.
 * @param x The number to be truncated.
 * @param places The number of places to truncate to.
 */
function truncate(x: number, places: number): number {
    const m = Math.pow(10, places)
    return Math.floor(x * m) / m
}

function update(state: GameState, {dt}: GameTime): GameState {
    state.theta += dt / 10
    return state
}

function render({ctx, theta}: GameState, {dt}: GameTime) {
    ctx.fillStyle = '#888'
    ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height)

    const { width, height } = dimensions(ctx)
    const r = Math.min(width, height) / 4
    const size = 50
    const x = r * Math.cos(theta) + (width - size) / 2
    const y = r * -Math.sin(theta) + (height - size) / 2

    ctx.font = '25px Consolas, monospace'
    ctx.fillStyle = 'white'
    ctx.fillText(`dt: ${truncate(dt, 4)}`, 50, 50)
    ctx.fillText(`fps: ${Math.ceil(1 / dt)}`, 50, 75)

    ctx.beginPath()
    ctx.strokeStyle = 'white'
    ctx.lineWidth = 2
    ctx.arc(width / 2, height / 2, r, 0, 2 * Math.PI)
    ctx.stroke()
    
    ctx.fillStyle = 'black'
    ctx.fillRect(x, y, size, size)
}

;(function main() {
    const canvas = createCanvas()
    const ctx = canvas.getContext('2d')
    coverScreen(ctx)
    window.addEventListener('resize', _ => coverScreen(ctx))

    const looper = new GameLooper(update, render, GameState.initial(ctx))
    looper.start()
})()
