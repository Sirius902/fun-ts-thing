export function createCanvas(): HTMLCanvasElement {
    let canvas = document.createElement('canvas')
    document.body.append(canvas)
    return canvas
}

/**
 * Resizes the context's canvas to cover the screen and not look
 * awful on high dpi screens.
 * @param ctx The context being used for the canvas.
 */
export function coverScreen(ctx: CanvasRenderingContext2D) {
    let canvas = ctx.canvas
    const dpr = window.devicePixelRatio || 1
    canvas.width = window.innerWidth * dpr
    canvas.height = window.innerHeight * dpr
    canvas.style.width = window.innerWidth + 'px'
    canvas.style.height = window.innerHeight + 'px'
    ctx.scale(dpr, dpr)
}

export interface Dimensions {
    width: number,
    height: number
}

export function dimensions(ctx: CanvasRenderingContext2D): Dimensions {
    const width = ctx.canvas.clientWidth
    const height = ctx.canvas.clientHeight    

    return { width, height }
}
