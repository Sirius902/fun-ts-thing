import GameTime from './GameTime.js'

/**
 * A game loop runner. Will run a render loop as frequently as possible
 * and an update loop at a customizable frequency, both are provided a time delta.
 * 
 * Designed around the great article at https://gafferongames.com/post/fix_your_timestep/.
 */
export default class GameLooper<S> {
    private running: boolean = false
    private time: number = 0
    private updateTime: number = 0
    private accumulator: number = 0

    constructor(
        private update: (state: S, gt: GameTime) => S,
        private render: (state: S, gt: GameTime) => void,
        private state: S,
        private dt: number = 1 / 60) {
    }

    private get gameTime(): GameTime {
        return new GameTime(this.updateTime, this.dt)
    }

    private tick(now: number) {
        if (this.running) {
            requestAnimationFrame(this.tick.bind(this))
        }

        const frameTime = Math.min(now - this.time, 0.25)

        this.time = now
        this.accumulator += frameTime

        while (this.accumulator >= this.dt) {
            this.state = this.update(this.state, this.gameTime)
            this.updateTime += this.dt
            this.accumulator -= this.dt
        }

        this.render(this.state, this.gameTime)
    }

    start() {
        requestAnimationFrame(this.tick.bind(this))
        this.running = true
    }

    stop() {
        this.running = false
    }
}
