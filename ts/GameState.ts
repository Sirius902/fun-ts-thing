export default class GameState {
    constructor(
        public ctx: CanvasRenderingContext2D,
        public theta: number) {
    }

    static initial(ctx: CanvasRenderingContext2D) {
        return new GameState(ctx, 0)
    }
}
